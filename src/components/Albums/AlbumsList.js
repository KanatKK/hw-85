import React from 'react';
import {NavLink} from "react-router-dom";

const AlbumsList = props => {
    return (
        <div className="album">
            <div className="albumImage">
                <img src={'http://localhost:8000/uploads/' + props.image} alt="" className="albumImage"/>
            </div>
            <p className="albumName" id={props.id}>
                <NavLink
                    style={{
                        color: '#000000',
                        textDecoration: 'underline',
                        border: 'none',
                        background: 'none',
                        fontSize: '15px',
                    }}
                    to={`/tracks/${props.id}`}
                >
                {props.name}
                </NavLink>
            </p>
            <p className="albumYear">
                Year: {props.year}
            </p>
        </div>
    );
};

export default AlbumsList;