import React from 'react';
import {useSelector} from "react-redux";
import {addTrackHistory} from "../../store/actions";

const TrackList = props => {
    const user = useSelector(state => state.getUser.user);

    const addTrackHistoryHandler = async event => {
        if (user !== null && user.username !== undefined) {
            const headers = {
                'Authentication': user.token,
            }
            await addTrackHistory({track: event.target.id, artist: props.artist}, headers);
        }
    };
    return (
        <div className="track">
            <div className="trackNumber">
                {props.number}
            </div>
            <div className="trackInfo">
                <h4>{props.name}</h4>
                <p>{props.duration}</p>
            </div>
            <div className="player" id={props.id} onClick={addTrackHistoryHandler}>
                ►
            </div>
        </div>
    );
};

export default TrackList;