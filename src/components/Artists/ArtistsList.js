import React from 'react';
import {NavLink} from "react-router-dom";

const ArtistsList = props => {
    return (
        <div className="artist">
            <div className="artistImage">
                <img src={'http://localhost:8000/uploads/' + props.image} alt="" className="artistImage"/>
            </div>
            <p className="artistName" id={props.id}>
                <NavLink
                    style={{
                        color: '#000000',
                        textDecoration: 'underline',
                        border: 'none',
                        background: 'none',
                        fontSize: '15px',
                    }}
                    to={`/albums/${props.id}`}
                >
                    {props.name}
                </NavLink>
            </p>
        </div>
    );
};

export default ArtistsList;