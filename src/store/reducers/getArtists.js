import {GET_ARTISTS} from "../actionTypes";

const initialState = {
    artists: null,
};

const getArtists = (state = initialState, action) => {
    switch (action.type) {
        case GET_ARTISTS:
            return {...state, artists: action.value};
        default:
            return state;
    }
};

export default getArtists;