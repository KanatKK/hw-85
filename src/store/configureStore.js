import {applyMiddleware, combineReducers, createStore} from "redux";
import getArtists from "./reducers/getArtists";
import getAlbums from "./reducers/getAlbums";
import getTracks from "./reducers/getTracks";
import createUser from "./reducers/createUser";
import getUser from "./reducers/getUser";
import getTrackHistory from "./reducers/getTrackHistory";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import thunkMiddleware from "redux-thunk";

const rootReducers = combineReducers({
    getArtists: getArtists,
    getAlbums: getAlbums,
    getTracks: getTracks,
    createUser: createUser,
    getUser: getUser,
    getTrackHistory: getTrackHistory,
});

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducers, persistedState, applyMiddleware(thunkMiddleware));

store.subscribe(() => {
    saveToLocalStorage({
        getUser: {
            user: store.getState().getUser.user
        }
    });
});

export default store;