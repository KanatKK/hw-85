import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists, fetchTracks} from "../../store/actions";
import Spinner from "../Spinner/Spinner";
import TrackList from "./TrackList";
import RegistrationBlock from "../Registration/RegistrationBlock";

const Tracks = props => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.getArtists.artists);
    const tracks = useSelector(state => state.getTracks.tracks);

    useEffect(() => {
        dispatch(fetchArtists());
        dispatch(fetchTracks(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    if (tracks !== null) {
        let artistName = null
        if (artists !== null) {
            Object.keys(artists).forEach(key => {
                if (artists[key]._id === tracks[0].album.artist) {
                    artistName = artists[key].name;
                }
            });
        }
        const trackList = tracks.map((track, index) => {
            return (
                <TrackList
                    key={index} artist={artistName} number={track.number}
                    name={track.name} duration={track.duration} id={track._id}
                />
            );
        });
        return (
            <div className="container">
                <header>
                    <h2>Tracks</h2>
                    <RegistrationBlock/>
                </header>
                <div className="content">
                    <h2>{artistName}</h2>
                    <h3>{tracks[0].album.name && tracks[0].album.name}</h3>
                    <div className="albumImageForTracks">
                        <img
                            src={'http://localhost:8000/uploads/' + tracks[0].album.image}
                            alt="" className="albumImageForTracks"
                        />
                    </div>
                    <div className="tracks">
                        {trackList}
                    </div>
                </div>
            </div>
        );
    } else {
        return (<Spinner/>);
    }
};

export default Tracks;