import axios from "axios";
import {
    ADD_PASSWORD,
    ADD_USERNAME,
    GET_ALBUMS,
    GET_ARTISTS,
    GET_TRACK_HISTORY,
    GET_TRACKS,
    GET_USER
} from "./actionTypes";

const getArtistsList = value => {
    return {type: GET_ARTISTS, value};
};

const getAlbumsList = value => {
    return {type: GET_ALBUMS, value};
};

const getTrackList = value => {
    return {type: GET_TRACKS, value};
};

export const fetchUser = value => {
    return {type: GET_USER, value};
};

export const getTrackHistory = value => {
    return {type: GET_TRACK_HISTORY, value};
};

export const addUserName = value => {
    return {type: ADD_USERNAME, value};
};

export const addPassword = value => {
    return {type: ADD_PASSWORD, value};
};

export const fetchArtists = () => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/artists');
            dispatch(getArtistsList(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const fetchAlbums = (artistId) => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/albums?artist=' + artistId);
            dispatch(getAlbumsList(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const fetchTracks = (albumId) => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/tracks?album=' + albumId);
            dispatch(getTrackList(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const createUser = (data) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8000/users', data);
            dispatch(fetchUser(response.data))
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchUser(e.response.data));
            }
        }
    };
};

export const registerUser = (data) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8000/users/sessions', data);
            dispatch(fetchUser(response.data));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchUser(e.response.data));
            }
        }
    };
};

export const fetchTrackHistory = (user) => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/track_history/' + user);
            dispatch(getTrackHistory(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const addTrackHistory = async (track, headers) => {
    try {
        await axios.post('http://localhost:8000/track_history', track, {headers: headers});
    } catch (e) {
        console.log(e);
    }
};

export const logOutUser = () => {
    return async (dispatch, getState) => {
        const token = getState().getUser.user.token;
        const headers = {"Authorization": token};
        await axios.delete('http://localhost:8000/users/sessions', {headers});
        dispatch(fetchUser(null));
    }
};