import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addPassword, addUserName, createUser} from "../../store/actions";
import {NavLink} from "react-router-dom";

const SignUp = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.getUser.user);
    const userName = useSelector(state => state.createUser.username);
    const password = useSelector(state => state.createUser.password);
    const userData = useSelector(state => state.createUser);

    const toLogIn = () => {
        props.history.push("/login");
    };

    const addUserNameHandler = event => {
        dispatch(addUserName(event.target.value));
    };

    const addPasswordHandler = event => {
        dispatch(addPassword(event.target.value));
    };

    const createUserHandler = () => {
        dispatch(createUser(userData));
        dispatch(addUserName(""));
        dispatch(addPassword(""));
    };

    if (user !== null && user.username) {
        props.history.push("/");
    }

    return (
        <div className="container">
            <header>
                <h2>Registration</h2>
                <span className="nav">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to={'/'}
                    >
                        Artists
                    </NavLink>
                </span>
            </header>
            <div className="registration">
                <input
                    type="text" value={userName}
                    className="userName" placeholder="User Name"
                    onChange={addUserNameHandler}
                />
                <input
                    type="text" value={password}
                    className="password" placeholder="Password"
                    onChange={addPasswordHandler}
                />
                <button onClick={createUserHandler} type="button" className="regButton">Create new account</button>
                {user && <span className="error">{user.error}</span>}
                <p onClick={toLogIn}>Already have an account? Login!</p>
            </div>
        </div>
    );
};

export default SignUp;