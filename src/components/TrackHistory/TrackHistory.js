import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchTrackHistory, logOutUser} from "../../store/actions";
import RegistrationBlock from "../Registration/RegistrationBlock";
import Moment from "react-moment";
import {NavLink} from "react-router-dom";

const TrackHistory = props => {
    const dispatch = useDispatch();
    const trackHistory = useSelector(state => state.getTrackHistory.trackHistory);
    const user = useSelector(state => state.getUser.user);

    const logOut = () => {
        dispatch(logOutUser());
    };

    useEffect(() => {
        dispatch(fetchTrackHistory(props.match.params.user));
    },[dispatch, props.match.params.user]);

    if (user === null) {
        props.history.push('/login');
    }

    if (trackHistory !== null) {
        const trackHistoryList = trackHistory.map((history, index) => {
            return (
                <div key={index} className="history">
                    <p>{history.artist} - {history.track}</p>
                    <p>At: <Moment>{history.time}</Moment></p>
                </div>
            );
        });
        return (
            <div className="container">
                <header>
                    <h2>Track History</h2>
                    <span className="nav">
                        <NavLink
                            style={{
                                color: 'black',
                                textDecoration: 'underline',
                                cursor: 'pointer',
                                marginRight: 10,
                            }}
                            to={'/'}
                        >
                        Artists
                    </NavLink>
                        <span className="name" onClick={logOut}>
                        <NavLink
                            style={{
                                color: 'black',
                                textDecoration: 'underline',
                                cursor: 'pointer'
                            }}
                            to="/"
                        >{user.username}</NavLink>
                        </span>
                    </span>
                </header>
                <div className="content">
                    {trackHistoryList}
                </div>
            </div>
        );
    } else {
        return (
            <div className="container">
                <header>
                    <h2>Track History</h2>
                    <RegistrationBlock/>
                </header>
            </div>
        );
    }
};

export default TrackHistory;