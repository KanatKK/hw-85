import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addPassword, addUserName, registerUser} from "../../store/actions";
import {NavLink} from "react-router-dom";

const LogIn = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.getUser.user);
    const userName = useSelector(state => state.createUser.username);
    const password = useSelector(state => state.createUser.password);
    const userData = useSelector(state => state.createUser);

    const addUserNameHandler = event => {
        dispatch(addUserName(event.target.value));
    };

    const addPasswordHandler = event => {
        dispatch(addPassword(event.target.value));
    };

    const registerUserHandler = () => {
        dispatch(registerUser(userData));
        dispatch(addUserName(""));
        dispatch(addPassword(""));
    };

    if (user !== null && user.username) {
        props.history.push("/");
    }

    return (
        <div className="container">
            <header>
                <h2>Registration</h2>
                <span className="nav">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to={'/'}
                    >
                        Artists
                    </NavLink>
                </span>
            </header>
            <div className="registration">
                <input
                    type="text" value={userName}
                    className="userName" placeholder="User Name"
                    onChange={addUserNameHandler}
                />
                <input
                    type="text" value={password}
                    className="password" placeholder="Password"
                    onChange={addPasswordHandler}
                />
                <button type="button" onClick={registerUserHandler} className="regButton">Login</button>
                {user && <span className="error">{user.error}</span>}
            </div>
        </div>
    );
};

export default LogIn;