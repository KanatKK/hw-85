export const GET_ARTISTS = "GET_ARTISTS";
export const GET_ALBUMS = "GET_ALBUMS";
export const GET_TRACKS = "GET_TRACKS";
export const GET_USER = "GET_USER";
export const GET_TRACK_HISTORY = "GET_TRACK_HISTORY";

export const ADD_USERNAME = "ADD_USERNAME";
export const ADD_PASSWORD = "ADD_PASSWORD";