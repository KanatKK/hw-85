import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions";
import Spinner from "../Spinner/Spinner";
import ArtistsList from "./ArtistsList";
import RegistrationBlock from "../Registration/RegistrationBlock";

const Artists = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.getArtists.artists);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    if (artists) {
        const artistsList = artists.map((artist, index) => {
            return(
                <ArtistsList
                    key={index} name={artist.name} image={artist.image} id={artist._id}
                />
            );
        });
        return (
            <div className="container">
                <header>
                    <h2>Artists</h2>
                    <RegistrationBlock/>
                </header>
                <div className="content">
                    <div className="artists">
                        {artistsList}
                    </div>
                </div>
            </div>
        );
    } else {
        return(<Spinner/>);
    }
};

export default Artists;